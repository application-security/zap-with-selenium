package com.ZAP_Selenium;

import java.util.concurrent.TimeUnit;
import org.openqa.selenium.WebDriver;
import com.ZAP_Selenium_ReadObjectRepository.Locator;

public class Login {

	WebDriver driver;
	// Main Directory of the project
	String currentDir = System.getProperty("user.dir");
	// Global locator file
	Locator locator = new Locator(currentDir + "//objectRepository.properties");

	final static String BASE_URL = "https://owasp.org/www-project-juice-shop/";
	final static String LOGOUT_URL = "http://localhost:8080/bodgeit/logout.jsp";
	final static String USERNAME = "test101@demo.com";
	final static String PASSWORD = "demotest";

	public Login(WebDriver driver) {
		this.driver = driver;
		this.driver.manage().timeouts().pageLoadTimeout(10, TimeUnit.SECONDS);
		this.driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	}

	// navigation before login
	public void navigateBeforeLogin() throws Exception {
		driver.findElement(locator.getLocator("challenges")).click();
	}

	// login
	public void loginAsUser() throws Exception {
		driver.findElement(locator.getLocator("login")).click();
		driver.findElement(locator.getLocator("username_field")).sendKeys(USERNAME);
		driver.findElement(locator.getLocator("password_field")).sendKeys(PASSWORD);
		driver.findElement(locator.getLocator("login_button")).click();
		verifyPresenceOfText("Successfully logged in");
	}

	// navigation after login
	public void navigateAfterLogin() throws Exception {
		driver.findElement(locator.getLocator("scoreboard")).click();
	}

	//Verify the page title must contain expected text 
	public void verifyPresenceOfText(String text) {
		String pageSource = this.driver.getPageSource();
		if (!pageSource.contains(text))
			throw new RuntimeException("Expected text: [" + text + "] was not found.");
	}
}
